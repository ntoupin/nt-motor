/**
 * @file NT_Motor.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Motor function definitions.
 */

#include "Arduino.h"
#include "NT_Motor.h"

 /**
  * @brief Declaration of Motor_t class
  */
Motor_t::Motor_t()
{
}


/**
 * @brief This function sets the internal parameters of the motor (Setpoint (SP) -> Internal Processing -> Operation Point (OP)).
 * @param Startup_Sp Value of the setpoint when the block is initialized.
 * @param Deadband_Sp Necessary value variation of the SP to change the OP.
 * @param Minimum_Sp Lower value that the setpoint can reach. If entered value is below this, it will be set to this value.
 * @param Maximum_Sp High value that the setpoint can reach. If entered value is above this, it will be set to this value.
 * @param Minimum_Op Minimum value of output.
 * @param Maximum_Op Maximum value of output.
 * @return Nothing
 */
void Motor_t::Configure(int Startup_Sp, int Deadband_Sp, int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op)
{
	_Startup_Sp = Startup_Sp;
	_Deadband_Sp = Deadband_Sp;
	_Minimum_Sp = Minimum_Sp;
	_Maximum_Sp = Maximum_Sp;
	_Minimum_Op = Minimum_Op;
	_Maximum_Op = Maximum_Op;
}

/**
 * @brief This functions enables the motor.
 * @return Nothing
 */
void Motor_t::Init()
{
	Enable = TRUE;
}

/**
 * @brief This functions disables the motor.
 * @return Nothing
 */
void Motor_t::Deinit()
{
	Enable = FALSE;
}

/**
 * @brief This function computes de output value of the motor.
 * @param Setpoint Disired setpoint within the limits.
 * @return Nothing
 */
void Motor_t::Set(int Setpoint)
{
	if (Enable == TRUE)
	{
		// Check if difference between new and old value is more than the defined deadband
		int Difference = abs(Value_Sp - Setpoint);

		// Set new pwm value if difference is more than defined deadband
		if (Difference >= _Deadband_Sp)
		{	
			// Roof data if below or over limits
			if (Setpoint > _Maximum_Sp)
			{
				Value_Sp = _Maximum_Sp;
			}
			else if (Setpoint < _Minimum_Sp)
			{
				Value_Sp = _Minimum_Sp;
			}
			else
			{
				Value_Sp = Setpoint;
			}
		}
	}
	else
	{
		Value_Sp = _Startup_Sp;
	}

	// Map value of setpoint to correct duty cycle values
	if (Value_Sp > ((_Maximum_Sp - _Minimum_Sp) / 2)) // Forward
	{
		Value1_Op = 0;
		Value2_Op = map(Value_Sp, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op);
	}
	else if (Value_Sp < ((_Maximum_Sp - _Minimum_Sp) / 2)) // Reverse
	{
		Value1_Op = abs(map(Value_Sp, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op));
		Value2_Op = 0;
	}
	else
	{
		Value1_Op = 0;
		Value2_Op = 0;
	}
}