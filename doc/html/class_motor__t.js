var class_motor__t =
[
    [ "Motor_t", "class_motor__t.html#afb2404b56ac6475700273466b7c85d11", null ],
    [ "Configure", "class_motor__t.html#af3d12f25293a55dd338e9cd8cd81007f", null ],
    [ "Deinit", "class_motor__t.html#a4205ebfcf8b209adc3643669ea5d9a24", null ],
    [ "Init", "class_motor__t.html#ab56f7027ac774755109ac2e530ee5e42", null ],
    [ "Set", "class_motor__t.html#a2ccbc5d03bc019ec165fc0add215e861", null ],
    [ "Deadband_Sp", "class_motor__t.html#a10784fcab948aca60c9b7c0d8834190b", null ],
    [ "Enable", "class_motor__t.html#a426107cec876f58d1e1f16d47e9a922f", null ],
    [ "Maximum_Op", "class_motor__t.html#ac2ca65fec05ccef123fc71038845aa71", null ],
    [ "Maximum_Sp", "class_motor__t.html#a5e730f7ab724853990891b7c500b538d", null ],
    [ "Minimum_Op", "class_motor__t.html#a788757624539ad15ebd58e1c576661bb", null ],
    [ "Minimum_Sp", "class_motor__t.html#a3daa1f1705c1301a123a21ed4c7ddc6b", null ],
    [ "Startup_Sp", "class_motor__t.html#a79f15a8baf1c154d1730c0fd28afb9f9", null ],
    [ "Value1_Op", "class_motor__t.html#a79ee9bf95dec57c2d2d6351f9844bfcd", null ],
    [ "Value2_Op", "class_motor__t.html#a552b4f36c5dc0d66e62fbd83ee0994ef", null ],
    [ "Value_Sp", "class_motor__t.html#ae5ed637deae388ed7dee4121465588a9", null ]
];