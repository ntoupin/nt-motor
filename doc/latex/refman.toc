\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Class Index}{1}% 
\contentsline {section}{\numberline {1.1}Class List}{1}% 
\contentsline {chapter}{\numberline {2}File Index}{3}% 
\contentsline {section}{\numberline {2.1}File List}{3}% 
\contentsline {chapter}{\numberline {3}Class Documentation}{5}% 
\contentsline {section}{\numberline {3.1}Motor\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Class Reference}{5}% 
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{6}% 
\contentsline {subsection}{\numberline {3.1.2}Constructor \& Destructor Documentation}{6}% 
\contentsline {subsubsection}{\numberline {3.1.2.1}Motor\_t()}{6}% 
\contentsline {subsection}{\numberline {3.1.3}Member Function Documentation}{6}% 
\contentsline {subsubsection}{\numberline {3.1.3.1}Configure()}{6}% 
\contentsline {subsubsection}{\numberline {3.1.3.2}Deinit()}{7}% 
\contentsline {subsubsection}{\numberline {3.1.3.3}Init()}{7}% 
\contentsline {subsubsection}{\numberline {3.1.3.4}Set()}{7}% 
\contentsline {subsection}{\numberline {3.1.4}Member Data Documentation}{8}% 
\contentsline {subsubsection}{\numberline {3.1.4.1}Deadband\_Sp}{8}% 
\contentsline {subsubsection}{\numberline {3.1.4.2}Enable}{8}% 
\contentsline {subsubsection}{\numberline {3.1.4.3}Maximum\_Op}{8}% 
\contentsline {subsubsection}{\numberline {3.1.4.4}Maximum\_Sp}{9}% 
\contentsline {subsubsection}{\numberline {3.1.4.5}Minimum\_Op}{9}% 
\contentsline {subsubsection}{\numberline {3.1.4.6}Minimum\_Sp}{9}% 
\contentsline {subsubsection}{\numberline {3.1.4.7}Startup\_Sp}{9}% 
\contentsline {subsubsection}{\numberline {3.1.4.8}Value1\_Op}{9}% 
\contentsline {subsubsection}{\numberline {3.1.4.9}Value2\_Op}{10}% 
\contentsline {subsubsection}{\numberline {3.1.4.10}Value\_Sp}{10}% 
\contentsline {chapter}{\numberline {4}File Documentation}{11}% 
\contentsline {section}{\numberline {4.1}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/ntoupin/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Arduino/libraries/nt-\/motor/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Keywords.txt File Reference}{11}% 
\contentsline {section}{\numberline {4.2}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/ntoupin/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Arduino/libraries/nt-\/motor/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}N\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}T\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Motor.cpp File Reference}{11}% 
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{11}% 
\contentsline {section}{\numberline {4.3}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/ntoupin/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Arduino/libraries/nt-\/motor/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}N\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}T\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Motor.h File Reference}{11}% 
\contentsline {subsection}{\numberline {4.3.1}Detailed Description}{12}% 
\contentsline {subsection}{\numberline {4.3.2}Macro Definition Documentation}{12}% 
\contentsline {subsubsection}{\numberline {4.3.2.1}FALSE}{12}% 
\contentsline {subsubsection}{\numberline {4.3.2.2}NT\_Motor\_h}{12}% 
\contentsline {subsubsection}{\numberline {4.3.2.3}TRUE}{12}% 
\contentsline {chapter}{Index}{13}% 
