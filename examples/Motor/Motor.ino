/*
   @file Motor.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of motor utilisation.
*/

#include <NT_Pwm.h>
#include <NT_Motor.h>

Pwm_t Motor1_Pwm1;
Pwm_t Motor1_Pwm2;
Motor_t Motor1;

void setup()
{
  // Definition : Configure(int Startup_Sp, int Deadband_Sp, int Minimum_Sp, int Maximum_Sp,  int Minimum_Op, int Maximum_Op);
  Motor1.Configure(0, 0, 0, 1000, 0, 256);
  Motor1.Init();

  Motor1_Pwm1.Configure(0, 0, Motor1.Minimum_Op, Motor1.Maximum_Op, 0, 255);
  Motor1_Pwm1.Attach(9);
  Motor1_Pwm1.Init();

  Motor1_Pwm2.Configure(0, 0, Motor1.Minimum_Op, Motor1.Maximum_Op, 0, 255);
  Motor1_Pwm2.Attach(10);
  Motor1_Pwm2.Init();
}

void loop()
{
  for (int i = Motor1.Minimum_Sp; i < Motor1.Maximum_Sp; i += 20)
  {
    Motor1.Set(i);
    Motor1_Pwm1.Set(Motor1.Value1_Op);
    Motor1_Pwm2.Set(Motor1.Value2_Op);

    delay(10);
  }
}
