/**
 * @file NT_Motor.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Motor function declarations.
 */

#ifndef NT_Motor_h

#include "Arduino.h"
#include "NT_Pwm.h"

#define TRUE 1
#define FALSE 0

#define NT_Motor_h

 /**
  * @brief This class contains all the necessary stuff to control a motor.
  */
class Motor_t
{
public:
	/// Motor object
	Motor_t();

	/// State of the motor
	bool Enable = FALSE;

	/// Current setpoint.
	int Value_Sp = 0;

	/// Value of the first pwm.
	int Value1_Op = 0;

	/// Value of the second pwm.
	int Value2_Op = 0;

	/// Configure the settings of the motor.
	void Configure(int Startup_Sp, int Deadband_Sp, int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op);

	/// Initialize the motor.
	void Init();

	/// Dinitialize the motor.
	void Deinit();

	/// Set the Setpoint of the motor.
	void Set(int Setpoint);

private:

	/// Value of the setpoint at startup.
	int _Startup_Sp = 0;

	/// Value of the deadband.
	int _Deadband_Sp = 0;

	/// Value of the minimum setpoint.
	int _Minimum_Sp = 0;

	/// Value of the maximum setpoint.
	int _Maximum_Sp = 0;

	/// Value of the minimum operating point.
	int _Minimum_Op = 0;

	/// Value of the maximum operating point.
	int _Maximum_Op = 0;
	
};

#endif